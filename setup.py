#!/usr/bin/env python3
# ------------------------------------------------------------------------------
#  Copyleft 2020  PacMiam
#
#  This program is free software: you can redistribute it and/or modify it
#  under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful, but WITHOUT
#  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
#  License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
# ------------------------------------------------------------------------------

from setuptools import setup, find_packages


# ------------------------------------------------------------------------------
#   Setup
# ------------------------------------------------------------------------------

setup(
    name='gravel',
    version='0.1.0',
    author='PacMiam',
    author_email='pacmiam@tuxfamily.org',
    description='Little community platform',
    long_description='Gravel is a community platform website based on Django.',
    keywords='community platform',
    url='https://framagit.org/kawateam/gravel',
    project_urls={
        'Source': 'https://framagit.org/kawateam/gravel',
        'Tracker': 'https://framagit.org/kawateam/gravel/issues',
    },
    packages=find_packages(exclude=['test']),
    include_package_data=True,
    python_requires='~= 3.6',
    install_requires=[
        'Django ~= 3.0',
        'Markdown',
        'Pygments',
    ],
    extras_require={
        'dev': [
            'pytest',
            'flake8',
        ],
    },
    entry_points={
        'console_scripts': [
            'gravel-ctl = gravel.__main__:main',
        ],
    },
    license='AGPLv3',
    classifiers=[
        'Development Status :: 1 - Planning',
        'Environment :: Web Environment',
        'Framework :: Django :: 3.1',
        'Intended Audience :: End Users/Desktop',
        'License :: OSI Approved :: '
            'GNU Affero General Public License v3 or later (AGPLv3+)',
        'Natural Language :: French',
        'Operating System :: POSIX :: Linux',
        'Programming Language :: Python :: 3 :: Only',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
        'Topic :: Internet :: WWW/HTTP :: Browsers',
    ],
)
