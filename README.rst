Gravel
======
An other Django-related project for my friends, yeah!

This project used the GNU Affero General Public License.

Dependencies
------------
In order to use Gravel, the following dependencies are necessary:

- Python 3.6+
- Django 3.0+
- Python-Markdown 3.2.0+
- Python-Pygments 2.6.0+

Django, Markdown and Pygments can be installed through ``pip``.

Installation
------------
Retrieve Gravel source code with ``git`` ::

    git clone https://framagit.org/PacMiam/gravel ~/gravel

You can use a Python virtual environment to install Gravel::

    cd ~/gravel
    python3 -m virtualenv ~/gravel-venv
    source ~/gravel-venv/bin/activate
    ptyhon3 -m pip install .

Running
-------
When Gravel is installed with ``pip``, the main program can be called with
``gravel-ctl`` command.

To configure Gravel, you can specify a file path with ``GRAVEL_CONFIG``
environment variable::

    GRAVEL_CONFIG="~/gravel.ini" gravel-ctl runserver 127.0.0.1:8001
