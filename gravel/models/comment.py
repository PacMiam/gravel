# ------------------------------------------------------------------------------
#  Copyleft 2020  PacMiam
#
#  This program is free software: you can redistribute it and/or modify it
#  under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful, but WITHOUT
#  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
#  License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
# ------------------------------------------------------------------------------

# Django
from django.forms import ModelForm
from django.utils.translation import gettext as _

# Gravel
from gravel.models.abstract import ContentAbstractModel


# ------------------------------------------------------------------------------
#   Models
# ------------------------------------------------------------------------------

class Comment(ContentAbstractModel):

    class Meta():
        # Default sorting order for models data
        ordering = ["created_date"]


class CommentForm(ModelForm):

    class Meta():
        model = Comment

        fields = ["content"]

    def __init__(self, *args, **kwargs):
        """ Constructor
        """

        super().__init__(*args, **kwargs)

        self.fields["content"].widget.attrs.update(
            placeholder=_("Write a comment"), rows="4")
