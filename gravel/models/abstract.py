# ------------------------------------------------------------------------------
#  Copyleft 2020  PacMiam
#
#  This program is free software: you can redistribute it and/or modify it
#  under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful, but WITHOUT
#  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
#  License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
# ------------------------------------------------------------------------------

# Django
from django.contrib.auth.models import User
from django.db import models
from django.utils.translation import gettext as _


# ------------------------------------------------------------------------------
#   Models
# ------------------------------------------------------------------------------

class SimpleAbstractModel(models.Model):

    created_date = models.DateTimeField(auto_now_add=True,
                                        verbose_name=_("Created date"))
    updated_date = models.DateTimeField(auto_now=True,
                                        verbose_name=_("Updated date"))

    author = models.ForeignKey(User,
                               on_delete=models.CASCADE,
                               related_name='+',
                               verbose_name=_("Author"))
    editor = models.ForeignKey(User,
                               blank=True,
                               null=True,
                               on_delete=models.CASCADE,
                               related_name='+',
                               verbose_name=_("Editor"))

    class Meta():
        # Ensure to set abstract status
        abstract = True

    def __str__(self):
        """ Represents the child model as string

        Returns
        -------
        str
            Child model representation
        """

        return f"#{self.pk} by {self.author.username}"


class ContentAbstractModel(SimpleAbstractModel):

    content = models.TextField(verbose_name=_("Content"))

    class Meta():
        # Ensure to set abstract status
        abstract = True
