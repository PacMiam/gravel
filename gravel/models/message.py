# ------------------------------------------------------------------------------
#  Copyleft 2020  PacMiam
#
#  This program is free software: you can redistribute it and/or modify it
#  under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful, but WITHOUT
#  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
#  License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
# ------------------------------------------------------------------------------

# Django
from django.db import models
from django.urls import reverse
from django.utils.text import slugify
from django.utils.translation import gettext as _

# Gravel
from gravel.models.abstract import ContentAbstractModel
from gravel.models.comment import Comment


# ------------------------------------------------------------------------------
#   Models
# ------------------------------------------------------------------------------

class MessageAbstractModel(ContentAbstractModel):

    slug = models.SlugField(editable=False,
                            max_length=96,
                            null=True)

    title = models.CharField(max_length=96,
                             verbose_name=_("Title"))

    comments = models.ManyToManyField(Comment,
                                      blank=True,
                                      verbose_name=_("Comments"))

    success_url_string = None

    class Meta():
        # Ensure to set abstract status
        abstract = True

    def __str__(self):
        """ Represents the child model as string

        Returns
        -------
        str
            Child model representation
        """

        return f"#{self.pk} - {self.slug} by {self.author.username}"

    def get_absolute_url(self):
        """ Override django.db.models.Model.get_absolute_url
        """

        return reverse(
            self.success_url_string, kwargs={"pk": self.pk, "slug": self.slug})

    def delete(self, *args, **kwargs):
        """ Override django.db.models.Model.delete
        """

        for comment in self.comments.all():
            comment.delete()

        super().delete(*args, **kwargs)

    def save(self, *args, **kwargs):
        """ Override save method to generate a slug field
        """

        self.slug = slugify(self.title)

        super().save(*args, **kwargs)
