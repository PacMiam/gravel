# ------------------------------------------------------------------------------
#  Copyleft 2020  PacMiam
#
#  This program is free software: you can redistribute it and/or modify it
#  under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful, but WITHOUT
#  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
#  License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
# ------------------------------------------------------------------------------

# Django
from django import template
from django.template.defaultfilters import stringfilter

# Markdown
from markdown import markdown


# ------------------------------------------------------------------------------
#   Tags
# ------------------------------------------------------------------------------

register = template.Library()


@register.filter
@stringfilter
def to_markdown(text):
    """ Convert text to markdown

    Parameters
    ----------
    text : str
        Text to convert

    Returns
    -------
    str
        Converted text
    """

    return markdown(text,
                    extensions=["codehilite", "extra", "nl2br"],
                    extension_configs={
                        "codehilite": {
                            "linenums": False,
                        }
                    })
