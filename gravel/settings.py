# ------------------------------------------------------------------------------
#  Copyleft 2020  PacMiam
#
#  This program is free software: you can redistribute it and/or modify it
#  under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful, but WITHOUT
#  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
#  License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
# ------------------------------------------------------------------------------

# Filesystem
from configparser import ConfigParser
from pathlib import Path

# System
from os import environ


# ------------------------------------------------------------------------------
#   Gravel
# ------------------------------------------------------------------------------

gravel_config_path = environ.get("GRAVEL_CONFIG", None)

if gravel_config_path is None:
    raise FileNotFoundError("Configuration file must be define with "
                            "GRAVEL_CONFIG environment variable")

gravel_config = ConfigParser()
gravel_config.read(gravel_config_path)

# ------------------------------------------------------------------------------
#   Paths
# ------------------------------------------------------------------------------

# If not None, define the SCRIPT_NAME HTTP header
FORCE_SCRIPT_NAME = gravel_config.get("server", "url", fallback=None)

# Define the base URL
BASE_URL = '/' if FORCE_SCRIPT_NAME is None else FORCE_SCRIPT_NAME
# L’URL ou le motif d’URL nommé vers lequel seront redirigées les requêtes pour
# la connexion avec le décorateur login_required() ou les classes
# LoginRequiredMixin et AccessMixin.
LOGIN_URL = f"{BASE_URL}accounts/login/"
# L’URL ou le motif d’URL nommé vers lequel seront redirigées les requêtes après
# la connexion si la vue LoginView ne reçoit pas de paramètre GET next.
LOGIN_REDIRECT_URL = BASE_URL
# L’URL ou le motif d’URL nommé vers lequel seront redirigées les requêtes après
# la déconnexion si la vue LogoutView ne possède pas d’attribut next_page.
LOGOUT_REDIRECT_URL = LOGIN_URL
# URL utilisée pour se référer aux fichiers statiques se trouvant dans
# STATIC_ROOT.
STATIC_URL = f"{BASE_URL}static/"

#
BASE_DIR = Path(__file__).resolve().parent.parent
LOCALE_DIR = str(BASE_DIR.joinpath("gravel", "locale"))
STATIC_DIR = str(BASE_DIR.joinpath("gravel", "static"))

# Une liste de répertoires dans lesquels Django cherche les fichiers de
# traduction.
LOCALE_PATHS = [LOCALE_DIR]
# Ce réglage définit les emplacements supplémentaires que l’application
# staticfiles parcourt si la recherche par FileSystemFinder est activée, par
# exemple quand vous utilisez la commande de gestion collectstatic ou
# findstatic, ou quand vous utilisez la vue de service de fichiers statiques.
STATICFILES_DIRS = [STATIC_DIR]

# Le chemin absolu vers le répertoire dans lequel collectstatic rassemble les
# fichiers statiques en vue du déploiement.
STATIC_ROOT = gravel_config.get("path", "static")

# ------------------------------------------------------------------------------
#   Settings
# ------------------------------------------------------------------------------

allowed_hostname = gravel_config.get("server", "allowed_hostname", fallback="")

# Allowed hosts on website
ALLOWED_HOSTS = ["localhost", "127.0.0.1", "[::1]"]
for hostname in allowed_hostname.split():
    ALLOWED_HOSTS.append(hostname)

# Language
LANGUAGE_CODE = gravel_config.get("django", "language", fallback="en")
USE_I18N = gravel_config.getboolean("django", "use_i18n", fallback=True)
USE_L10N = gravel_config.getboolean("django", "use_l10n", fallback=True)

# Time zone
TIME_ZONE = gravel_config.get("django", "timezone", fallback="UTC")
USE_TZ = gravel_config.getboolean("django", "use_timezone", fallback=True)

# Error output options
DEBUG = gravel_config.getboolean("django", "activate_debug", fallback=False)

# Session options
SESSION_EXPIRE_AT_BROWSER_CLOSE = gravel_config.getboolean(
    "django", "session_expire_at_browser_close", fallback=False)

CSRF_COOKIE_SECURE = gravel_config.getboolean(
    "server", "csrf_cookie_secure", fallback=True)
SECURE_SSL_REDIRECT = gravel_config.getboolean(
    "server", "secure_ssl_redirect", fallback=True)
SESSION_COOKIE_SECURE = gravel_config.getboolean(
    "server", "session_cookie_secure", fallback=True)
USE_X_FORWARDED_HOST = gravel_config.getboolean(
    "server", "use_x_forwarded_host", fallback=True)

SECRET_KEY = gravel_config.get("database", "secret")
if not len(SECRET_KEY):
    raise KeyError("Cannot use an empty secret key")

# ------------------------------------------------------------------------------
#   Applications
# ------------------------------------------------------------------------------

INSTALLED_APPS = [
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.humanize",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    "gravel",
]

MIDDLEWARE = [
    "django.middleware.security.SecurityMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
]

ROOT_URLCONF = "gravel.urls"
WSGI_APPLICATION = "gravel.wsgi.application"

# ------------------------------------------------------------------------------
#   Templates
# ------------------------------------------------------------------------------

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [
            str(BASE_DIR.joinpath("gravel", "templates")),
        ],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
            ],
        },
    },
]

# ------------------------------------------------------------------------------
#   Database
# ------------------------------------------------------------------------------

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.sqlite3",
        "NAME": str(BASE_DIR.joinpath("db.sqlite3")),
    }
}

# ------------------------------------------------------------------------------
#   Password
# ------------------------------------------------------------------------------

AUTH_PASSWORD_VALIDATORS = [
    {
        "NAME": "django.contrib.auth.password_validation."
                "UserAttributeSimilarityValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation."
                "MinimumLengthValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation."
                "CommonPasswordValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation."
                "NumericPasswordValidator",
    }
]
