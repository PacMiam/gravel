# ------------------------------------------------------------------------------
#  Copyleft 2020  PacMiam
#
#  This program is free software: you can redistribute it and/or modify it
#  under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful, but WITHOUT
#  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
#  License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
# ------------------------------------------------------------------------------

# Django
from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.urls import path
from django.conf.urls import include

# Gravel
from gravel.views.comment import CommentUpdateView
from gravel.views.forum import (MessageView, MessagesView, MessageCreateView,
                                MessageUpdateView, MessageDeleteView)
from gravel.views.wiki import (ArticleView, ArticlesView, ArticleCreateView,
                               ArticleUpdateView, ArticleDeleteView)
from gravel.views.home import HomeView

# ------------------------------------------------------------------------------
#   Patterns
# ------------------------------------------------------------------------------

urlpatterns = [
    # Home
    path('', HomeView.as_view(), name="home"),
    # Authentication
    path(r"accounts/", include([
        path(r"login/",
             auth_views.LoginView.as_view(),
             name="login"),
        path(r"logout/",
             auth_views.LogoutView.as_view(),
             name="logout"),
    ])),
    # Forum
    path(r"forum/", include([
        path(r"",
             MessagesView.as_view(),
             name="messages-list"),
        path(r"message/<str:slug>-<int:pk>/",
             MessageView.as_view(),
             name="message-detail"),
        path(r"add/",
             MessageCreateView.as_view(),
             name="message-create"),
        path(r"edit/<int:pk>/",
             MessageUpdateView.as_view(),
             name="message-update"),
        path(r"delete/<int:pk>/",
             MessageDeleteView.as_view(),
             name="message-delete"),
    ])),
    # Wiki
    path(r"wiki/", include([
        path(r"",
             ArticlesView.as_view(),
             name="articles-list"),
        path(r"article/<str:slug>-<int:pk>/",
             ArticleView.as_view(),
             name="article-detail"),
        path(r"add/",
             ArticleCreateView.as_view(),
             name="article-create"),
        path(r"edit/<int:pk>/",
             ArticleUpdateView.as_view(),
             name="article-update"),
        path(r"delete/<int:pk>/",
             ArticleDeleteView.as_view(),
             name="article-delete"),
    ])),
    # Comment
    path(r"comment/", include([
        path(r"edit/<int:pk>/",
             CommentUpdateView.as_view(),
             name="comment-update"),
    ])),
    # Django related
    path(r"admin/", admin.site.urls),
]
