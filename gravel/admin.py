# ------------------------------------------------------------------------------
#  Copyleft 2020  PacMiam
#
#  This program is free software: you can redistribute it and/or modify it
#  under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful, but WITHOUT
#  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
#  License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
# ------------------------------------------------------------------------------
# Django
from django.contrib import admin

# Robusta
from gravel.models.comment import Comment
from gravel.models.forum import Message
from gravel.models.wiki import Article


# ------------------------------------------------------------------------------
#   Manager
# ------------------------------------------------------------------------------

admin.site.register(Article)
admin.site.register(Comment)
admin.site.register(Message)
