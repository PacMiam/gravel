# ------------------------------------------------------------------------------
#  Copyleft 2020  PacMiam
#
#  This program is free software: you can redistribute it and/or modify it
#  under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful, but WITHOUT
#  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
#  License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
# ------------------------------------------------------------------------------

# Django
from django.urls import reverse_lazy

# Robusta
from gravel.models.comment import Comment
from gravel.views.abstract import AbstractDeleteView, AbstractUpdateView


# ------------------------------------------------------------------------------
#   Class
# ------------------------------------------------------------------------------

class CommentUpdateView(AbstractUpdateView):
    model = Comment

    fields = ("content",)

    template_name = "common/form.html"

    permission_required = "gravel.change_comment"

    def get_success_url(self):
        """ Override django.views.generic.edit.ModelFormMixin.get_success_url
        """

        model_method = next((element for element in dir(self.object)
                             if element.endswith("_set")), None)
        if model_method is None:
            return reverse_lazy("home")

        model_object = getattr(self.object, model_method).first()
        return f"{model_object.get_absolute_url()}#{self.object.pk}"


class MessageDeleteView(AbstractDeleteView):
    model = Comment

    success_url = reverse_lazy("messages-list")

    template_name = "common/delete.html"

    permission_required = "gravel.delete_comment"
