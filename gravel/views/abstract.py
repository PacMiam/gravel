# ------------------------------------------------------------------------------
#  Copyleft 2020  PacMiam
#
#  This program is free software: you can redistribute it and/or modify it
#  under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful, but WITHOUT
#  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
#  License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
# ------------------------------------------------------------------------------

# Django
from django.contrib.auth.mixins import (LoginRequiredMixin,
                                        PermissionRequiredMixin)
from django.core.exceptions import PermissionDenied
from django.urls import reverse_lazy
from django.views.generic import DetailView, ListView
from django.views.generic.edit import (CreateView, DeleteView, FormMixin,
                                       UpdateView)


# ------------------------------------------------------------------------------
#   Class
# ------------------------------------------------------------------------------

class AbstractListView(LoginRequiredMixin, ListView):
    pass


class AbstractDetailView(LoginRequiredMixin, DetailView, FormMixin):

    def get_success_url(self):
        """ Override django.views.generic.edit.ModelFormMixin.get_success_url
        """

        if self.object.success_url_string is not None:
            return f"{self.object.get_absolute_url()}#latest"

        return reverse_lazy("home")

    def post(self, request, *args, **kwargs):
        """ Override django.views.generic.edit.ProcessFormView.post
        """

        if "gravel.add_comment" not in request.user.get_all_permissions():
            raise PermissionDenied

        self.object = self.get_object()

        form = self.get_form()
        if form.is_valid():
            return self.form_valid(form)

        return self.form_invalid(form)

    def form_valid(self, form):
        """ Override django.views.generic.edit.ModelFormMixin.form_valid
        """

        form.instance.author = self.request.user
        comment = form.save()

        self.object.comments.add(comment)
        self.object.save()

        return super().form_valid(form)


class AbstractCreateView(LoginRequiredMixin,
                         PermissionRequiredMixin, CreateView):

    def form_valid(self, form):
        """ Override django.views.generic.edit.ModelFormMixin.form_valid
        """

        form.instance.author = self.request.user

        return super().form_valid(form)


class AbstractUpdateView(LoginRequiredMixin,
                         PermissionRequiredMixin, UpdateView):

    def post(self, request, *args, **kwargs):
        """ Override django.views.generic.edit.ProcessFormView.post
        """

        self.object = self.get_object()

        form = self.get_form()
        if form.is_valid():
            return self.form_valid(form)

        return self.form_invalid(form)

    def form_valid(self, form):
        """ Override django.views.generic.edit.ModelFormMixin.form_valid
        """

        form.instance.editor = self.request.user

        return super().form_valid(form)

    def get_object(self, *args, **kwargs):
        """ Override django.views.generic.detail.SingleObjectMixin.get_object
        """

        item = super().get_object(*args, **kwargs)
        if self.request.user.is_staff or item.author == self.request.user:
            return item

        raise PermissionDenied


class AbstractDeleteView(LoginRequiredMixin,
                         PermissionRequiredMixin, DeleteView):

    def get_object(self, *args, **kwargs):
        """ Override django.views.generic.detail.SingleObjectMixin.get_object
        """

        item = super().get_object(*args, **kwargs)
        if self.request.user.is_staff or item.author == self.request.user:
            return item

        raise PermissionDenied
