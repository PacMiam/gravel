# ------------------------------------------------------------------------------
#  Copyleft 2020  PacMiam
#
#  This program is free software: you can redistribute it and/or modify it
#  under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful, but WITHOUT
#  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
#  License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
# ------------------------------------------------------------------------------

# Django
from django.contrib.auth.mixins import LoginRequiredMixin
from django.utils.translation import gettext as _
from django.views.generic.base import TemplateView

# Gravel
from gravel.models.forum import Message
from gravel.models.wiki import Article


# ------------------------------------------------------------------------------
#   Class
# ------------------------------------------------------------------------------

class HomeView(LoginRequiredMixin, TemplateView):
    template_name = "home.html"

    def get_latest_elements(self, model, max_items, detail_view, list_view):
        """ Retrieve latest elements for a specific model

        Parameters
        ----------
        model : django.db.models.Model
            Model to fetch
        max_items : int
            Maximum items to fetch from specified model
        detail_view : str
            Detail view url string
        list_view : str
            List view url string

        Returns
        -------
        dict
            Model context for homepage
        """

        elements = list()
        for element in model.objects.all():
            elements.append(element)

            for index, comment in enumerate(element.comments.all(), start=1):
                comment.index = index
                comment.parent = element
                elements.append(comment)

        elements = sorted(elements, key=lambda k: k.created_date, reverse=True)

        return {
            "elements": elements[:max_items],
            "detail": detail_view,
            "list": list_view,
        }

    def get_context_data(self, **kwargs):
        """ Override django.views.generic.base.ContextMixin.get_context_data
        """

        elements = {
            _("Articles"): (Article, 10, "article-detail", "articles-list"),
            # _("Reports"): (Report, 0, "report-detail", "reports-list"),
            _("Messages"): (Message, 10, "message-detail", "messages-list"),
        }

        context = super().get_context_data(**kwargs)

        # Ensure to sort elements key by translated names
        context["elements"] = dict()
        for key in sorted(elements.keys()):
            context["elements"][key] = self.get_latest_elements(*elements[key])

        return context
