# ------------------------------------------------------------------------------
#  Copyleft 2020  PacMiam
#
#  This program is free software: you can redistribute it and/or modify it
#  under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful, but WITHOUT
#  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
#  License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
# ------------------------------------------------------------------------------

# Django
from django.urls import reverse_lazy

# Gravel
from gravel.models.comment import CommentForm
from gravel.models.wiki import Article
from gravel.views.abstract import (AbstractCreateView, AbstractDeleteView,
                                   AbstractDetailView, AbstractListView,
                                   AbstractUpdateView)


# ------------------------------------------------------------------------------
#   Class
# ------------------------------------------------------------------------------

class ArticlesView(AbstractListView):
    model = Article
    paginate_by = 20

    template_name = "wiki/articles.html"


class ArticleView(AbstractDetailView):
    model = Article
    form_class = CommentForm

    template_name = "wiki/article.html"

    query_pk_and_slug = False
    slug_url_kwarg = "article"


class ArticleCreateView(AbstractCreateView):
    model = Article

    fields = ("title", "content")

    template_name = "common/form.html"

    permission_required = "gravel.add_article"


class ArticleUpdateView(AbstractUpdateView):
    model = Article

    fields = ("title", "content")

    template_name = "common/form.html"

    permission_required = "gravel.change_article"


class ArticleDeleteView(AbstractDeleteView):
    model = Article

    success_url = reverse_lazy("articles-list")

    template_name = "wiki/delete.html"

    permission_required = "gravel.delete_article"
